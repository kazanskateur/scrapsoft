@extends('layouts.app')

@section('page-title')
    Wishlists Dashboard
@endsection

@section('content')
<main role="main">

    <div class="album py-5 bg-light">
      <div class="container">

        <div class="row">


          @forelse ($wishlists as $wishlist)
            <div class="col-md-4">
                {{--Changer l'url ciblé !--}}
                <a class="text-reset text-decoration-none" href="show/{{$wishlist->id}}">

                    <div class="card mb-4">
                        <div class="card-body mb-3">
                          <h5 class="text-decoration-underline" class="card-text">{{$wishlist->name}}</h5>
                          {{--<p class="card-text">{{$wishlist->description}}</p>--}}
                          <div class="d-flex justify-content-between align-items-center">
                          </div>
                        </div>
                  </div>
                </a>
            </div>
          @empty
            <h3>Vous n'avez pas de wishlist</h3>
          @endforelse
            <a class="text-reset text-decoration-none" href={{ route('wishlist.create') }}>
                <div class="card mb-4">
                    <div class="card-body mb-3 text-center">
                      <h5 class="card-text">+</h5>
                      {{--<p class="card-text">{{$wishlist->description}}</p>--}}
                      <div class="d-flex justify-content-between align-items-center">
                      </div>
                    </div>
                </div>
            </a>
        </div>
      </div>
    </div>

  </main>
@endsection


@section('css')
@endsection

@section('scripts')
@endsection
