@extends('layouts.app')

@section('page-title')
    {{$wishlist->name}}
@endsection

@section('content')
<main class="d-block mx-auto">
    <a href='{{ route('wishlist.showUpdate', ["wishlist" => $wishlist]) }}'>
        <button class="btn btn-secondary">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-gear" viewBox="0 0 16 16">
                <path d="M8 4.754a3.246 3.246 0 1 0 0 6.492 3.246 3.246 0 0 0 0-6.492zM5.754 8a2.246 2.246 0 1 1 4.492 0 2.246 2.246 0 0 1-4.492 0z"/>
                <path d="M9.796 1.343c-.527-1.79-3.065-1.79-3.592 0l-.094.319a.873.873 0 0 1-1.255.52l-.292-.16c-1.64-.892-3.433.902-2.54 2.541l.159.292a.873.873 0 0 1-.52 1.255l-.319.094c-1.79.527-1.79 3.065 0 3.592l.319.094a.873.873 0 0 1 .52 1.255l-.16.292c-.892 1.64.901 3.434 2.541 2.54l.292-.159a.873.873 0 0 1 1.255.52l.094.319c.527 1.79 3.065 1.79 3.592 0l.094-.319a.873.873 0 0 1 1.255-.52l.292.16c1.64.893 3.434-.902 2.54-2.541l-.159-.292a.873.873 0 0 1 .52-1.255l.319-.094c1.79-.527 1.79-3.065 0-3.592l-.319-.094a.873.873 0 0 1-.52-1.255l.16-.292c.893-1.64-.902-3.433-2.541-2.54l-.292.159a.873.873 0 0 1-1.255-.52l-.094-.319zm-2.633.283c.246-.835 1.428-.835 1.674 0l.094.319a1.873 1.873 0 0 0 2.693 1.115l.291-.16c.764-.415 1.6.42 1.184 1.185l-.159.292a1.873 1.873 0 0 0 1.116 2.692l.318.094c.835.246.835 1.428 0 1.674l-.319.094a1.873 1.873 0 0 0-1.115 2.693l.16.291c.415.764-.42 1.6-1.185 1.184l-.291-.159a1.873 1.873 0 0 0-2.693 1.116l-.094.318c-.246.835-1.428.835-1.674 0l-.094-.319a1.873 1.873 0 0 0-2.692-1.115l-.292.16c-.764.415-1.6-.42-1.184-1.185l.159-.291A1.873 1.873 0 0 0 1.945 8.93l-.319-.094c-.835-.246-.835-1.428 0-1.674l.319-.094A1.873 1.873 0 0 0 3.06 4.377l-.16-.292c-.415-.764.42-1.6 1.185-1.184l.292.159a1.873 1.873 0 0 0 2.692-1.115l.094-.319z"/>
              </svg>
        </button>
    </a>
    <table class="table table-bordered w-75 py-5 px-3">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Price</th>
            <th scope="col">Priority</th>
            <th scope="col">Site</th>
            <th scope="col">Disponible</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @forelse ($wishlist->products as $product)
              <tr>
                <th scope="row">{{$product->name}}</th>
                <td>{{$product->price}}</td>
                <td>{{$product->priority}}</td>
                <td>{{$product->site->name}}</td>
                @if($product->available)
                    <td class='table-success table-bordered'></td>
                @else
                    <td class='table-danger'></td>
                @endif
                <td><a href='{{ route('wishlist.deleteProduct', ["wishlist" => $wishlist->id,"product"=> $product->id]) }}'><button class="btn btn-primary"> Delete </button></a></td>
              </tr>
          @empty

          @endforelse
        </tbody>
    </table>
    {{--Ajout de produit--}}
    <form  method='post' action='{{ route('wishlist.addProduct',$wishlist->id) }}'>
        @csrf
        <div class='card w-75 py-5 px-3'>
            <div class="form-group col-md-12 mt-3">
                <label for="product-url">Lien produit</label>
                <input type="text" name='url' class="form-control" id="product-url" placeholder="Entrer le lien du produit" value="{{ old('product-url') }}"/>
                @error('product-url')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
                @enderror
            </div>
            <div class="form-group col-md-12 mt-3">
                <label for="site-select">Site du produit</label>
                <select id='site-select' name='site_id' class="form-select">
                        <option selected>Select site</option>
                    @forelse ($sites as $site)
                        <option value="{{$site->id}}">{{$site->name}}</option>
                    @empty
                        <p>Probleme dans les sites disponibles rafraichissez</p>
                    @endforelse
                </select>
                @error('site-select')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group col-md-12 mt-3">
                <label for="priority-select">Priorité du produit</label>
                <select id='priority-select' name='priority' class="form-select">
                        <option selected>1</option>
                    @for ($i=2;$i<=5;$i++)
                        <option value="{{$i}}">{{$i}}</option>
                    @endfor
                </select>
                @error('priority-select')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group col-md-12 mt-3">
                <button type="submit" class="btn btn-primary">Ajouter</button>
            </div>
        </div>
    </form>
</main>


@endsection

@section('css')
@endsection

@section('scripts')
    <script>
        window.onload=function () {
            document.getElementById('product-url').addEventListener("input", (event) => {
                var sites = document.getElementById('site-select').options;
                var i;
                for (i = 0; i < sites.length; i++) {
                    if(document.getElementById('product-url').value.includes(sites[i].text)) document.getElementById("site-select").value = sites[i].value;
                }
            });
        }



    </script>
@endsection
