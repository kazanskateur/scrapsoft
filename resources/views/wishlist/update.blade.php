@extends('layouts.app')

@section('page-title')
    Wishlist Update
@endsection

@section('content')
    @include('wishlist.form')
@endsection


@section('css')
@endsection

@section('scripts')
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
@endsection
