<form method="post" class="vstack gap-2">
    @csrf
    <div class="form-group">
        <label for="name">Nom</label>
        <input type="string" class="form-control @error("name") is-invalid @enderror" id="name" name="name" value="{{ old('name', $wishlist->name) }}">
        @error("name")
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <input type="text" class="form-control @error("description") is-invalid @enderror" id="description" name="description" value="{{ old('description', $wishlist->description) }}">
        @error("description")
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button class="btn btn-primary">
        @if($wishlist->id)
            Modifier
        @else
            Creer
        @endif
    </button>
</form>
