@extends('layouts.app')

@section('page-title')
    Wishlists Dashboard
@endsection

@section('content')
<main role="main">

    <div class="album py-5 bg-light">
      <div class="container">

        <div class="row">


            @foreach($sites as $site)
                <div class="col-md-4">
                        <div class="card mb-4">
                            <div class="card-body mb-3">
                              <h5 class="text-decoration-underline" class="card-text">{{$site->name}}</h5>
                              <div class="d-flex justify-content-between align-items-center">
                                {{-- @foreach($site->products as $product)
                                    <p>{{$product->name}}</p>
                                @endforeach --}}
                                {{$site->className()}}
                              </div>
                            </div>
                      </div>
                    </a>
                </div>
            @endforeach



        </div>
      </div>
    </div>

  </main>
@endsection


@section('css')
@endsection

@section('scripts')
@endsection
