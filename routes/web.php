<?php

use App\Http\Controllers\WishlistController;
use App\Http\Controllers\SiteController;
use Illuminate\Support\Facades\Route;
use App\Models\Site;


// AJOUTER LES MIDDLEWARES SUR LES ROUTES DE MODIF DE PLAYLIST
//Routes necessitant d'etre auth
Route::middleware(['auth'])->group(function () {
    //Entry point
    Route::get('/', function () {return redirect()->route('wishlist.index');})->name('welcome');
    Route::get('/dump', function () {
        $price = SiteController::getAvailibility('taiwangun','https://www.taiwangun.com/fr/bille-020g-6mm/airsoft-bb-pellets-0-23g-bio-4350pcs-polished-precise-seamless-s-t');
         return view('dump',compact('price'));});

    Route::get('/sites/index',[SiteController::class,'index']);
    //Routes concernant les playlists
    Route::prefix('/wishlist')->name('wishlist.')->controller(WishlistController::class)->group(function(){
        Route::get('/index','index')->name('index');

        Route::get('/create','showCreate')->name('showCreate');
        Route::post('/create','create')->name('create');
        Route::get('/update/{wishlist}','showUpdate')->name('showUpdate');
        Route::post('/update/{wishlist}','update')->name('update');

        Route::get('/show/{wishlist}','show')->name('show');
        Route::post('/add_product/{wishlist}','addProduct')->name('addProduct');
        Route::get('/delete_product/{wishlist}/{product}','deleteProduct')->name('deleteProduct');


        //Route::get('/reset_musics/{playlist}','resetMusics')->name('reset_musics');
        // Route::get('/create','showCreate');

        // Route::post('/create','create');
        // Route::post('/update/{playlist}','update');
        });


    });
