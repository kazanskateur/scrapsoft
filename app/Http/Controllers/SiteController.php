<?php

namespace App\Http\Controllers;

use App\Models\Site;
use App\Models\Sites\AirsoftentrepotSite;
use App\Models\Sites\Powair6Site;
use App\Models\Sites\TaiwangunSite;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    // public static function nameExtract(String $url,String $site){
    //     $name = '';
    //     return $name;
    // }
    public function index(Request $request){
        $sites = Site::all();
        return view('sites.index',compact('sites'));
    }


    public static function getPrice($siteName, $productUrl): float{
        $price = 0;
        switch ($siteName) {
            case 'taiwangun':
              $price = TaiwangunSite::getPrice($productUrl);
              break;
            case 'powair6':
              $price = Powair6Site::getPrice($productUrl);
              break;
            case 'airsoft-entrepot':
              $price =  AirsoftentrepotSite::getPrice($productUrl);
              break;
            default:
              //raise error
          }
        return $price;
    }
    public static function getName($siteName, $productUrl): String{
        $name = '';
        switch ($siteName) {
            case 'taiwangun':
              $name = TaiwangunSite::getName($productUrl);
              break;
            case 'powair6':
              $name = Powair6Site::getName($productUrl);
              break;
            case 'airsoft-entrepot':
              $name =  AirsoftentrepotSite::getName($productUrl);
              break;
            default:
              //raise error
          }
        return $name;
    }
    public static function getAvailibility($siteName, $productUrl): bool{
        switch ($siteName) {
            case 'taiwangun':
              return TaiwangunSite::getAvailibility($productUrl);
              break;
            case 'powair6':
              return Powair6Site::getAvailibility($productUrl);
              break;
            case 'airsoft-entrepot':
              return  AirsoftentrepotSite::getAvailibility($productUrl);
              break;
            default:
              return false;
          }
    }
}
