<?php

namespace App\Http\Controllers;

use App\Models\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Site;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Barryvdh\Debugbar\Facades\Debugbar;
use Exception;
use App\Http\Requests\WishlistRequest;
class WishlistController extends Controller
{
    public function index(){
        $wishlists = Auth::user()->wishlists;
        return view('wishlist.index',compact('wishlists'));
    }
    public function show(Wishlist $wishlist, Request $request){
        $sites = Site::all();
        return view('wishlist.show',compact('wishlist','sites'));
    }

    public function showCreate(){
        $wishlist = new Wishlist();
        $wishlist->name = "Nom";
        $wishlist->description = "Description";
        return view('wishlist.create',compact('wishlist'));
    }
    public function showUpdate(Wishlist $wishlist){
        return view('wishlist.update',compact('wishlist'));
    }

    public function create(WishlistRequest $request){
        $wishlist = Wishlist::create($request->validated());
        //$wishlist->users()->save(Auth::user());
        return redirect()->route('wishlist.show',['wishlist' => $wishlist])->with('success',"La wishlist a bien été créée");

    }
    public function update(Wishlist $wishlist, WishlistRequest $request){
        $wishlist->update($request->validated());
        return redirect()->route('wishlist.show',['wishlist' => $wishlist])->with('success',"La wishlist a bien été modifiée");
    }




    public function addProduct(Wishlist $wishlist, ProductRequest $productRequest){
        $product = new Product();
        $product->url = $productRequest->validated()['url'];
        $product->priority = $productRequest->validated()['priority'];
        $product->site()->associate(Site::findOrfail($productRequest->validated()['site_id']));
        $product->wishlist()->associate($wishlist);

        try{
            $product->name = SiteController::getName($product->site->name,$product->url);
            $product->price = SiteController::getPrice($product->site->name,$product->url);
            $product->available = SiteController::getAvailibility($product->site->name,$product->url);
            $product->save();
            return  redirect()  ->route('wishlist.show',['wishlist' => $productRequest->validated()["wishlist_id"]])
                               ->with('success',"Le produit a bien été ajouté à la wishlist");
        }
        catch(Exception $e){
            return  redirect()  ->route('wishlist.show',['wishlist' => $productRequest->validated()["wishlist_id"]])
            ->with('error',"Une erreur est survenue");
        }

    }
    public function deleteProduct(Wishlist $wishlist, Product $product){
        $product->delete();
        return  redirect()  ->route('wishlist.show',['wishlist' => $wishlist])
        ->with('success',"Le produit a bien été suprimé de la wishlist");
    }
}
