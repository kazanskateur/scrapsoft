<?php

namespace App\Http\Requests;

use Barryvdh\Debugbar\Facades\Debugbar;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            //'name' => 'required|max:255',
            'url' => 'required|url',
            //'price' => 'required|numeric|min:0',
            'site_id' => 'required|exists:sites,id',
            'wishlist_id' => 'required|exists:wishlists,id',
            'priority' => 'required|integer|min:1|max:5',
        ];
    }

    protected function prepareForValidation(): void
    {
        Debugbar::info($this->route());
        $merge = [
                    'wishlist_id'       => $this->route()->parameter('wishlist')->id,
                ];
        $this->merge($merge);
    }
}
