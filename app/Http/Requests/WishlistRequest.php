<?php

namespace App\Http\Requests;

use App\Models\Wishlist;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class WishlistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|max:255',
            'description' => 'required',
            'owner_id' => 'required|integer',
            'room_key' => ['required',Rule::unique('wishlists')->ignore($this->route()->parameter('wishlist'))]
        ];
    }

    protected function prepareForValidation(): void
    {
        $merge = ['owner_id' => Auth::user()->id];
        if (null == $this->route()->parameter('wishlist')) {

            do{
                $val = rand(1000, 9999);
            }while(Wishlist::where('room_key',$val)->first() != null);
            $merge['room_key'] = $val;
        }else{
            $merge['room_key'] = $this->route()->parameter('wishlist')->room_key;
        }
        $this->merge($merge);
    }
}
