<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
class Wishlist extends Model
{
    protected $fillable = [
        'name',
        'description',
        'owner_id',
        'room_key',
    ];
    use HasFactory;
    public function owner(): BelongsTo
    {
        return $this->belongsTo(User::class,'owner_id');
    }
    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }
}
