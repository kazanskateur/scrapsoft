<?php declare(strict_types=1);

namespace App\Models;

use Barryvdh\Debugbar\Facades\Debugbar;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use GuzzleHttp\Client;
use DOMDocument;
use DOMXPath;

use aalfiann\PubProxy;

class Site extends Model
{
    use HasFactory;
    protected $table = 'sites';

    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }
    public function className(){
        return "Site";
    }

    public static function loadPage($url){

        $proxy = new PubProxy;
        $proxy->level = 'elite';
        $proxy->type = 'http';
        $proxy->country = 'us';
        $proxies = $proxy->make()->getProxy();
        $httpClient = new Client([
            "proxy" => [
                'http' => $proxies,
            ]
        ]);
        $response = $httpClient->get($url);
        Debugbar::info($response);
        $htmlString = (string) $response->getBody();
        //add this line to suppress any warnings
        libxml_use_internal_errors(true);
        $doc = new DOMDocument();
        $doc->loadHTML($htmlString);
        return new DOMXPath($doc);
    }

}
