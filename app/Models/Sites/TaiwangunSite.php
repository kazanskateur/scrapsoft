<?php

namespace App\Models\Sites;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Site;


class TaiwangunSite extends Site
{
    public static function getPrice($productUrl): float{
        $htmlPrice = Site::loadPage($productUrl);
        $nodes = $htmlPrice->query("//*[@class='price']");
        if($nodes[0] != null){
            return floatval($nodes[0]->textContent.PHP_EOL);
        }
        else{
            $nodes = $htmlPrice->query("//*[@itemprop='price']");
            return floatval(floatval($nodes[0]->getAttribute('content')));
        }
    }
    public static function getName($productUrl): string{
        {
            $htmlPrice = Site::loadPage($productUrl);
            $nodes = $htmlPrice->query("//*[@class='product-header page-header']");
            return $nodes[0]->textContent.PHP_EOL;
        }
    }
    public static function getAvailibility($productUrl): bool{
        {
            $htmlPrice = Site::loadPage($productUrl);
            $nodes = $htmlPrice->query("//*[@class='product-unavailable-label']");
            return count($nodes) == 0;
        }
    }


}
