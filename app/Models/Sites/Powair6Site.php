<?php

namespace App\Models\Sites;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Site;

class Powair6Site extends Site
{
    public static function getPrice($productUrl): float{
        $htmlPrice = Site::loadPage($productUrl);
        $nodes = $htmlPrice->query("//*[@class='price']");
        if($nodes[0] != null){
            return floatval(str_replace(',', '.', $nodes[0]->textContent.PHP_EOL));
        }
    }
    public static function getName($productUrl): string{
        {
            $htmlPrice = Site::loadPage($productUrl);
            $nodes = $htmlPrice->query("//*[@itemprop='name']");
            return $nodes[0]->textContent.PHP_EOL;

        }
    }
    public static function getAvailibility($productUrl): bool{
        {
            $htmlPrice = Site::loadPage($productUrl);
            $nodes = $htmlPrice->query("//*[@id='availability_value']");
            return str_contains($nodes[0]->getAttribute('class'),'success');
        }
    }

}
