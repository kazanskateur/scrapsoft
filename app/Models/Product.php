<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Http\Controllers\SiteController;
class Product extends Model
{
    use HasFactory;
    public function site(): BelongsTo
    {
        return $this->belongsTo(Site::class);
    }
    public function wishlist(): BelongsTo
    {
        return $this->belongsTo(Site::class);
    }
    public function getPrice(){
        SiteController::getPrice($this->site->name,$this->url);
    }
    public function getName(){
        SiteController::getName($this->site->name,$this->url);
    }
    public function getAvailibility(){
        SiteController::getAvailibility($this->site->name,$this->url);
    }
}
