<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $user = new User();
        $user -> name = "kaz";
        $user -> auth_name = "kaz";
        $user -> email ="hamon.kazan@u-psud.fr";
        $user -> password = Hash::make("password");
        $user -> admin = true;
        $user -> save();
    }
}
