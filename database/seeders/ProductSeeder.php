<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Site;
use App\Models\Wishlist;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $liste = ["replique","casque","gilet","consommable"];
        foreach(Wishlist::all() as $wishlist){
            foreach($liste as $produit){
                $site = Site::find(rand(1, 3));
                $product = new Product();
                $product->name = $produit;
                $product->price = rand(10, 100);
                $product->priority = rand(1, 5);
                $product->site()->associate($site);
                $product->wishlist()->associate($wishlist);
                $product->save();
            }

        }

    }
}
