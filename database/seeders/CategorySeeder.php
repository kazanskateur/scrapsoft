<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $liste = ['gear','accessoire','replique','upgrade'];
        foreach($liste as $cate){
            $categorie = new Category();
            $categorie->name = $cate;
            $categorie->save();
        }
    }
}
