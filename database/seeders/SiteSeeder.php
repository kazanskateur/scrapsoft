<?php

namespace Database\Seeders;

use App\Models\Sites\AirsoftentrepotSite;
use App\Models\Sites\Powair6Site;
use App\Models\Sites\TaiwangunSite;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Site;


class SiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $liste = ['taiwangun','airsoft-entrepot','powair6'];
        foreach($liste as $siteName){
            $site = new Site();
            $site->name = $siteName;
            $site->save();
        }

    }
}
