<?php

namespace Database\Seeders;

use App\Models\Wishlist;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;

class WishlistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for($i=1;$i<=3;$i++){
            $wishlist = new Wishlist();
            $wishlist->name = 'Wishlit '.strval($i);
            $wishlist->description = 'Description';
            do{
                $val = rand(1000, 9999);
            }while(Wishlist::where('room_key',$val)->first() != null);
            $wishlist->room_key = $val;
            $wishlist->owner()->associate(User::first());
            $wishlist->save();
        }
    }
}
