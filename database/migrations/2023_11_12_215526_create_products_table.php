<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('url')->default('lien');
            $table->float('price')->default(0);
            $table->integer('priority');
            $table->boolean('available')->default(false);
            $table->foreignId('wishlist_id')->constrained('wishlists')->onDelete('no action');
            $table->foreignId('site_id')->constrained('sites')->onDelete('no action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
